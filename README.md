# Ansible Playbook: Automated Server Configuration

This repository contains an Ansible playbook to automate the installation and configuration of Tomcat, PostgreSQL, and Certbot on Ubuntu servers.

## Prerequisites

- Ansible installed on your control node.
- SSH access to the target servers.
- sudo privileges on the target servers.

## Usage

1. Clone this repository to your local machine:

    ```bash
    git clone https://gitlab.com/Madhava.11/ansible-playbook-server-configuration.git
    ```

2. Navigate to the cloned directory:

    ```bash
    cd ansible-playbook-server-configuration
    ```

3. Run the following command to execute the playbook:

    ```bash
    ansible-playbook server_config.yml
    ```

## Playbook Structure

The playbook (`server_config.yml`) includes the following tasks:

1. Update package lists.
2. Install required packages (Java, PostgreSQL, Certbot).
3. Install Certbot snap package.
4. Download Tomcat tarball and extract it.
5. Create a Tomcat user.
6. Set ownership and permissions for the Tomcat directory.
7. Create a systemd service file for Tomcat.
8. Start the Tomcat service.
9. Enable the PostgreSQL service.
10. Reload systemd to apply changes.

## Additional Notes

- Make sure to replace `<repository_url>` with the actual URL of your GitHub repository.
- Update the `hosts` file with your target hosts or groups before executing the playbook.
- Ensure that your Ansible control node has SSH access to the target hosts and necessary sudo privileges.
- Depending on your specific setup and requirements, you may need to modify the playbook or template files.

